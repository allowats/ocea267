# Alex Watson
#------------------------OCEA 267: Problem Set 2------------------------------
# coded in python3

# import relevant modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf
from statsmodels.stats.stattools import durbin_watson
from scipy.stats import shapiro, bartlett

# 1. load PS2 dataset, plot chlorophyll concentration as a function of time
chloro = pd.read_csv("PS2_data-3.txt", delim_whitespace=True)

# plot
plt.figure()
plt.plot(chloro["dd"], chloro["y"])
plt.xlabel("Time (Year)")
plt.ylabel("Chlorophyll Concentration (mg/m^3)")
plt.title("Chlorophyll Concentration from 2011 to 2020")
plt.savefig("Watson_OCEA267_PS2_1.png", dpi=150)

# 2. Fit a multiple linear regression of cholophyll concentration as a 
#    function of time + seasonal cycle

# Create dummies
chloro = pd.get_dummies(chloro, columns=['month']) # will give 12 dummies, can ignore the 12th one

# Fit MLR
model = smf.ols("y ~ dd + month_1 + month_2 + month_3 + month_4 + month_5 + month_6 + month_7 + month_8 + month_9 + month_10 + month_11", data=chloro).fit()

# Plot model
plt.figure()
plt.plot(chloro["dd"], chloro["y"], label= "Observed Data")
plt.plot(chloro["dd"], model.fittedvalues, label = "Multiple Linear Regression")
plt.xlabel("Time (Year)")
plt.ylabel("Chlorophyll Concentration (mg/m^3)")
plt.title("Chlorophyll Concentration from 2011 to 2020")
plt.legend()
plt.savefig("Watson_OCEA267_PS2_2.png", dpi=150)

# 3. Plot the residuals of the above model
plt.figure()
plt.scatter(chloro["dd"], model.resid)
plt.plot(chloro["dd"], np.zeros(len(chloro["dd"])), "k--")
plt.xlabel("Time (Year)")
plt.ylabel("Model Residuals")
plt.title("Residuals of Multiple Linear Regression Model")
plt.savefig("Watson_OCEA267_PS2_3.png", dpi=150)

# 5. Update model with a dummy variable to correct for replacement of sensor and plot
# Dummy variable: 0 for t<2015, 1 for t>2015
chloro["calib"] = np.array(chloro["year"] >= 2015).astype(int)

# Update model
model2 = smf.ols("y ~ dd + month_1 + month_2 + month_3 + month_4 + month_5 + month_6 + month_7 + month_8 + month_9 + month_10 + month_11 + calib", data=chloro).fit()

# Plot new model
plt.figure()
plt.plot(chloro["dd"], chloro["y"], label= "Observed Data")
plt.plot(chloro["dd"], model.fittedvalues, label = "Multiple Linear Regression w/o Calibration")
plt.plot(chloro["dd"], model2.fittedvalues, label = "Multiple Linear Regression w/ Calibration")
plt.xlabel("Time (Year)")
plt.ylabel("Chlorophyll Concentration (mg/m^3)")
plt.title("Chlorophyll Concentration from 2011 to 2020")
plt.legend()
plt.savefig("Watson_OCEA267_PS2_5.png", dpi=150)

# 6. Plot the residuals of the new model
plt.figure()
plt.scatter(chloro["dd"], model2.resid)
plt.plot(chloro["dd"], np.zeros(len(chloro["dd"])), "k--")
plt.xlabel("Time (Year)")
plt.ylabel("Model Residuals")
plt.title("Residuals of Updated Multiple Linear Regression Model")
plt.savefig("Watson_OCEA267_PS2_6.png", dpi=150)

# 7. Perform tests on the residuals for normality, independence, variance
# Independence: perform Durbin-Watson Test
d = durbin_watson(model2.resid)
print("D = %f"%(d))
# value is between 1.5 and 2.5 -->independent 
# NOTE: durbin_watson does not report the p-value of this statistic in Python

# Normality: perform Shapiro-Wilk Test
s = shapiro(model2.resid)
print(" W = %f, p-value = %f"%(s[0], s[1]))
# The value is close to one and the p value is > 0.05 --> The residuals are normal

# Variance: perform Bartlett test
# break residuals into groups
res1 = model.resid[0:60:]
res2 = model.resid[61:]
# perform test
b_t = bartlett(res1, res2)
print("B = %f, p-value = %f"%(b_t[0],b_t[1]))
# The p-value is less than 0.05, which means the null hypothesis is rejected, nonconstant variance

# 9. Trend and Intervention Effect. Are they significant?
print("Trend = %f, Intervention Effect = %f"%(model2.params.dd,model2.params.calib))
print("Trend p-value = %f, Intervention p-value = %f"%(model2.pvalues.dd, model2.pvalues.calib))
# p-values are too small to show here, however they are viewable in Spyder's variable explorer
