# Alex Watson
#------------------------OCEA 267: Problem Set 3------------------------------
# coded in python3

# import relevant modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.stats.stattools import durbin_watson
from scipy.stats import shapiro, bartlett

# 1. Import the two datasets and plot their time series
# SST data
SST = pd.read_csv("SIO_SST.txt", delim_whitespace=True)

# Pacific Decadal Oscillation index
PDO = pd.read_csv("ersst.v5.pdo.dat", delim_whitespace=True)
# create annual mean column
anMean = []
for i in range(len(PDO.Year)):
    anMean.append(np.mean([PDO.Jan[i],PDO.Feb[i],PDO.Mar[i],PDO.Apr[i],PDO.May[i],
                       PDO.Jun[i],PDO.Jul[i],PDO.Aug[i],PDO.Sep[i],PDO.Oct[i],
                       PDO.Nov[i],PDO.Dec[i]]))
PDO["Mean"] = anMean

# Select Data from 1916 - 2020
PDO = PDO[PDO["Year"] >= 1916]
PDO = PDO[PDO["Year"] <= 2020]

# plot time series
SST = SST.set_index("year")
PDO = PDO.set_index("Year")


fig, (ax1, ax2) = plt.subplots(2,1,sharex=True)
ax1.plot(SST["SST"])
ax2.plot(PDO["Mean"])
ax1.set_ylabel("Annual Mean SST (C)")
ax2.set_ylabel("Annual Mean PDO")
ax2.set_xlabel("Year")
ax1.set_title("Change in SST and PDO over Time")
plt.savefig("Watson_OCEA267_PS3_1.png", dpi=150)
plt.show()

# 2. Plot PDO vs SST
plt.figure()
plt.scatter(PDO["Mean"],SST["SST"])
plt.xlabel("Annual Mean PDO")
plt.ylabel("Annual Mean SST (C)")
plt.title("Relationship between Annual Mean PDO and Annual Mean SST")
plt.savefig("Watson_OCEA267_PS3_2.png",dpi=150)
plt.show()

# 4. Fit linear regression model of SST as a function of PDO and plot regression
#    line to  scatterplot
x = PDO["Mean"]
X = sm.add_constant(x)
Y = SST["SST"]
linreg = sm.OLS(Y,X).fit()

# plot
plt.figure()
plt.scatter(x,Y)
plt.plot(x, linreg.fittedvalues, "r-", alpha = 0.7, label = "Linear Regression Model")
plt.xlabel("Annual Mean PDO")
plt.ylabel("Annual Mean SST (C)")
plt.title("Relationship between Annual Mean PDO and Annual Mean SST")
plt.legend()
plt.savefig("Watson_OCEA267_PS3_4.png",dpi=150)
plt.show()

# 5. Create scatterplot of residuals through time
plt.figure()
plt.scatter(linreg.resid.index, linreg.resid)
plt.plot(linreg.resid.index, np.zeros(len(linreg.resid.index)), 'k--', alpha = 0.5)
plt.xlabel("Year")
plt.ylabel("Residuals")
plt.title("Residuals of Linear Regression Model through Time")
plt.savefig("Watson_OCEA267_PS3_5.png",dpi=150)
plt.show()

# 7. Fit linear regression model of SST ~ PDO + Time
X["year"] = PDO.index
model = sm.OLS(Y,X).fit()

print(model.summary())
# Adj. R2 = 0.618
print(model.pvalues)

# 8. Plot the residuals of the updated model through time
plt.figure()
plt.scatter(model.resid.index, model.resid)
plt.plot(model.resid.index, np.zeros(len(model.resid.index)), 'k--', alpha = 0.5)
plt.xlabel("Year")
plt.ylabel("Residuals")
plt.title("Residuals of Updated Linear Regression Model through Time")
plt.savefig("Watson_OCEA267_PS3_8.png",dpi=150)
plt.show()

# 10. Add intervention dummy variable from 1988 onwards, fit model of SST ~ PDO
#     + Intervention
Int = np.array(SST.index >= 1988).astype(int)
X["Int"] = Int
model = sm.OLS(Y,X[["const","Mean","Int"]]).fit()

print(model.summary())
# adj R2 = 0.693
print(model.pvalues)
# significant p-values

# 11. Plot the residuals of the above model
plt.figure()
plt.scatter(model.resid.index, model.resid)
plt.plot(model.resid.index, np.zeros(len(model.resid.index)), 'k--', alpha = 0.5)
plt.xlabel("Year")
plt.ylabel("Residuals")
plt.title("Residuals of Intervention Linear Model through Time")
plt.savefig("Watson_OCEA267_PS3_11.png",dpi=150)
plt.show()

# 13. Fit a model of SST ~ PDO + time + intervention
model = sm.OLS(Y,X).fit()

print(model.summary())
# Adj. R2 = 0.703
print(model.pvalues)
# All components are significant minus the constant added to stabilize the 
# model originally

# 14. Plot the residuals of this final model
plt.figure()
plt.scatter(model.resid.index, model.resid)
plt.plot(model.resid.index, np.zeros(len(model.resid.index)), 'k--', alpha = 0.5)
plt.xlabel("Year")
plt.ylabel("Residuals")
plt.title("Residuals of Final Linear Model through Time")
plt.savefig("Watson_OCEA267_PS3_14.png",dpi=150)
plt.show()

# 15. Apply tests for independence, normality & variance
# Independence: perform Durbin-Watson Test
d = durbin_watson(model.resid)
print("D = %f"%(d))
# value is less than 1.5 --> some autocorrelation 
# NOTE: durbin_watson does not report the p-value of this statistic in Python

# Normality: perform Shapiro-Wilk Test
s = shapiro(model.resid)
print(" W = %f, p-value = %f"%(s[0], s[1]))
# The value is close to one and the p value is > 0.05 --> The residuals are normal

# Variance: perform Bartlett test
# break residuals into groups
res1 = model.resid[0:50:]
res2 = model.resid[51:]
# perform test
b_t = bartlett(res1, res2)
print("B = %f, p-value = %f"%(b_t[0],b_t[1]))
# The p-value > 0.05 --> Constant Variance