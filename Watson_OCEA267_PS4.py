# Alex Watson
#------------------------OCEA 267: Problem Set 4------------------------------
# coded in python3

# import relevant modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.tsa.stattools import pacf
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from statsmodels.tsa.ar_model import AutoReg

# 1. Import SIO_SST data, add a intervention dummy variable for t>1988, fit a 
#    linear regression model on that dummy alone and plot w/ the time series
# read data
SST = pd.read_csv("SIO_SST.txt", delim_whitespace=True)

# set up time series
SST = SST.set_index("year")

# create intervention dummy variable
Int = np.array(SST.index >= 1988).astype(int)

# flit linear regression model
X = sm.add_constant(Int)

model = sm.OLS(SST["SST"], X).fit()

# plot time series and fitted model
plt.figure()
SST["SST"].plot(label = "SST Time Series")
model.fittedvalues.plot(label = "Intervention Model")
plt.title("Change in SST over Time")
plt.xlabel("Time (Yr)")
plt.ylabel("SST (Degrees C)")
plt.legend()
plt.savefig("Watson_OCEA267_PS4_1.png", dpi=150)
plt.show()

# 2. Extract residuals from above model and plot them vs time

mresid = model.resid

# plot
plt.figure()
plt.plot(mresid.index, mresid)
plt.title("Adjusted SST Time Series")
plt.xlabel("Time (Yr)")
plt.ylabel("Residuals of Intervention Model")
plt.savefig("Watson_OCEA267_PS4_2.png", dpi=150)
plt.show()

# 3. Produce an ACF plot of adjusted SST time series
plt.figure()
plot_acf(mresid, title='ACF of Adjusted SST Time Series')
plt.savefig("Watson_OCEA267_PS4_3.png", dpi=150)
plt.show()

# 4. Produce a PACF plot of adjusted SST time series
plt.figure()
plot_pacf(mresid, title='PACF of Adjusted SST Time Series')
plt.savefig("Watson_OCEA267_PS4_4.png", dpi=150)
plt.show()

# 6. What is the lag-1 autocorrelation for your PACF?
PACF = pacf(mresid)

print("Lag-1 autocorrelation = %0.1f"%(PACF[1]))

# 7. Fit 1st-order autoregressive model to adjusted SST & report its AIC
AR = AutoReg(mresid, 1).fit()

# report AIC
print("AIC = %f"%(AR.aic))

# 8. Fit AR(15) model & report its AIC, compare to 7.
AR15 = AutoReg(mresid, np.linspace(1,15,1)).fit()

# AIC
print("AIC = %f"%(AR15.aic))
