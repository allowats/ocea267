# Alex Watson
# Week 3 In-Class Exercise (Lecture 6) part 2

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.formula.api as sf
from statsmodels.stats.stattools import durbin_watson


# read ch4 mm data
ch4 = pd.read_csv("ch4_mm_gl.txt" , sep = '\\s+')

avg = ch4['average']

#convert to time series
dates = []
for index, row in ch4.iterrows():
    dates.append(f'{int(row.year)}-{int(row.month)}')
dates = pd.to_datetime(dates)
ch4['date'] = dates
ch4.set_index('date', inplace=True)

#plot time series of monthly mean
ch4["average"].plot()
plt.xlabel('Time (yr)')
plt.ylabel('Monthly Mean CH4 (units?)')
plt.title('Atmospheric CH4 Monthly Mean Concentration Over Time')

# fit linear regression model & use dummy variables to model seasonal cycle, plot
# create seasonal dummy variables
# monthly --> 11 vars
ch4 = pd.get_dummies(ch4, columns=['month']) #gives 12 vars, can ignore the last column
# can use drop_first to remove 1st month instead of last month, dunno if this gives the same thing

# fit linear trend,season cycle
ch4['time'] = np.arange(0, len(ch4['year']),1)
linreg = sf.ols(formula="average ~ month_1+ month_2 + month_3 + month_4 + month_5 + month_6 + month_7 +  month_8 + month_9 + month_10 + month_11 + time", data=ch4).fit()

# plot
ch4["linreg"] = linreg.fittedvalues

plt.figure()
ch4["average"].plot(label="Monthly Mean")
ch4["linreg"].plot(label="Linear Regression Model")
plt.legend()
plt.xlabel("Time (Yr)")
plt.ylabel('Monthly Mean CH4 (units?)')
plt.title('Atmospheric CH4 Monthly Mean Concentration Over Time')
# doesn't fit data

# repeat w/ quadratic trend
ch4['time2'] = ch4['time']**2
quadreg = sf.ols(formula="average ~ month_1+ month_2 + month_3 + month_4 + month_5 + month_6 + month_7 +  month_8 + month_9 + month_10 + month_11 + time + time2", data=ch4).fit()

#plot
ch4["quadreg"] = quadreg.fittedvalues

plt.figure()
ch4["average"].plot(label="Monthly Mean")
ch4["linreg"].plot(label="Linear Regression Model")
ch4["quadreg"].plot(label='Quadratic Regression Model')
plt.legend()
plt.xlabel("Time (Yr)")
plt.ylabel('Monthly Mean CH4 (units?)')
plt.title('Atmospheric CH4 Monthly Mean Concentration Over Time')
# no improvement in fit

# repeat w/ cubic trend
ch4['time3'] = ch4['time']**3
cubreg = sf.ols(formula="average ~ month_1+ month_2 + month_3 + month_4 + month_5 + month_6 + month_7 +  month_8 + month_9 + month_10 + month_11 + time + time2 +time3", data=ch4).fit()

#plot
ch4["cubreg"] = cubreg.fittedvalues

plt.figure()
ch4["average"].plot(label="Monthly Mean")
ch4["linreg"].plot(label="Linear Regression Model")
ch4["quadreg"].plot(label='Quadratic Regression Model')
ch4["cubreg"].plot(label='Cubic Regression Model')
plt.legend()
plt.xlabel("Time (Yr)")
plt.ylabel('Monthly Mean CH4 (units?)')
plt.title('Atmospheric CH4 Monthly Mean Concentration Over Time')
# fits well

# Perform Durbin-Watson test on residuals from cubic trend fit
d = durbin_watson(cubreg.resid)
print(d)
# value is < 1.5 --> autocorrelation, residuals not independent