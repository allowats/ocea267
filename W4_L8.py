# Alex Watson
# Week 4 In-Class Exercise (Lecture 8)

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf
from scipy.stats import shapiro, bartlett

# read NY air quality data
nyaq = pd.read_csv("NYairqualitydata.txt", delim_whitespace=True, )
nyaq["Time"] = np.arange(1, len(nyaq["Ozone"])+1, 1)
nyaq = nyaq.set_index("Time")

# omit rows with na's
nyaq = nyaq.dropna()

# investigate all possible relationships between log(ozone) and temp, wind and 
# solar radiation by plotting all possible scatterplots of them

nyaq["logOzone"] = np.log10(nyaq["Ozone"])
axes = pd.plotting.scatter_matrix(nyaq[["logOzone","Solar.R","Wind","Temp"]])
plt.tight_layout()

plt.show

# fit a MLR of log(ozone) as a function of wind, temp, solar r.
# add quadratic terms
nyaq["qSolar"] = nyaq["Solar.R"]**2
nyaq["qWind"] = nyaq["Wind"]**2
nyaq["qTemp"] = nyaq["Temp"]**2
nyaq = nyaq.rename(columns={"Solar.R":"Solar"})
model = smf.ols("logOzone ~ Solar + Wind + Temp + Solar:Wind + Solar:Temp + Wind:Temp + Solar:Wind:Temp + qSolar + qWind + qTemp", data=nyaq).fit()

print(model.pvalues)

# start removing terms
# remove 3-way interaction
model = smf.ols("logOzone ~ Solar + Wind + Temp + Solar:Wind + Solar:Temp + Wind:Temp + qSolar + qWind + qTemp", data=nyaq).fit()

print(model.pvalues)

# remove Solar:Temp interaction
model = smf.ols("logOzone ~ Solar + Wind + Temp + Solar:Wind + Wind:Temp + qSolar + qWind + qTemp", data=nyaq).fit()

print(model.pvalues)

# remove Wind:Temp interaction
model = smf.ols("logOzone ~ Solar + Wind + Temp + Solar:Wind + qSolar + qWind + qTemp", data=nyaq).fit()

print(model.pvalues)

# remove solar:wind interaction
model = smf.ols("logOzone ~ Solar + Wind + Temp + qSolar + qWind + qTemp", data=nyaq).fit()

print(model.pvalues)

# remove Temp
model = smf.ols("logOzone ~ Solar + Wind + qSolar + qWind + qTemp", data=nyaq).fit()

print(model.pvalues)
print(model.summary())
# adj R2: 0.688
# Durbin-Watson: 1.732 --> likely independent residuals

# Apply Shapiro-Wilk test on residuals for normality
s_t = shapiro(model.resid)
print(s_t[1])
# the p-value is just above 0.05 so the assumption of normality likely cannot be rejected

# apply Bartlett test on residuals
res1 = model.resid[0:55]
res2 = model.resid[56:]
b_t = bartlett(res1, res2)
print(b_t[1])

# the p value is very low --> the residuals do not have constant variance

# fit model from exercise on Monday
model = smf.ols("logOzone ~ Solar + Wind + Temp + qWind + qTemp", data=nyaq).fit()

print(model.pvalues)
print(model.summary())
# adj R2 = 0.673
# my model has a higher R2 --> it's better