#Alex Watson

import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
# Linear Trend Sim Experiment

# Simulation scenario: Normal, independent & constant variance
n=100
x= np.random.normal(loc=0, scale=1.0, size = n)
t = np.arange(0,n,1)
model = sm.OLS(x,t).fit()

plt.figure()
plt.plot(t,x, label="raw data")
plt.plot(t, model.fittedvalues, label="trend")
plt.title('p-value= %0.1f'%(model.pvalues[0]))

# Scenario 2: normal, autocorrelated, constant variance
def sample_signal(n_samples, corr, mu=0, sigma=1):
    assert 0 < corr < 1, "Auto-correlation must be between 0 and 1"

    # Find out the offset `c` and the std of the white noise `sigma_e`
    # that produce a signal with the desired mean and variance.
    # See https://en.wikipedia.org/wiki/Autoregressive_model
    # under section "Example: An AR(1) process".
    c = mu * (1 - corr)
    sigma_e = np.sqrt((sigma ** 2) * (1 - corr ** 2))

    # Sample the auto-regressive process.
    signal = [c + np.random.normal(0, sigma_e)]
    for _ in range(1, n_samples):
        signal.append(c + corr * signal[-1] + np.random.normal(0, sigma_e))

    return np.array(signal)

phi = 0.5
y = sample_signal(n,phi)
model = sm.OLS(y,t).fit()

plt.figure()
plt.plot(t,y, label="raw data")
plt.plot(t, model.fittedvalues, label="trend")
plt.title('p-value= %0.1f'%(model.pvalues[0]))
