# Alex Watson
# Week 8 In-Class Exercise (Lecture 16)

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import statsmodels.api as sm
from statsmodels.stats.stattools import durbin_watson
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from scipy.stats import shapiro, bartlett

# Download SF temp data
SFT = pd.read_csv("sanfrancisco_tmax_1895_2022.csv",skiprows=4)

#remove month part from Date
dates = SFT.Date.values

for i in range(len(SFT.Date)):
    datestr = str(dates[i])
    size = len(datestr)
    mod_str = datestr[:size - 2]
    SFT.Date[i] = int(mod_str)
    
# Calculate and plot empirical CDF
x = SFT.Value.values
x.sort()
x = x[2:]
n = len(x)
num = np.linspace(1,n,n)
F_x = num/(n+1)

# plot
plt.figure()
plt.plot(x, F_x)
plt.xlabel("Temperature")
plt.ylabel("Empirical CDF")
plt.title("Empirical CDF of San Francisco Annual Maximum Temperature")
plt.show()

# fit generalized extreme value distribution & superimpose on empirical cdf
