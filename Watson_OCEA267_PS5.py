# Alex Watson
#------------------------OCEA 267: Problem Set 5------------------------------
# coded in python3

# import relevant modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

# 4. Import SIO_SST data, add a intervention dummy variable for t>1988, fit a 
#    linear regression model on that dummy alone and report magnitude and 
#    p-value of intervention effect
# read data
SST = pd.read_csv("SIO_SST.txt", delim_whitespace=True)

# set up time series
SST = SST.set_index("year")

# create intervention dummy variable
Int = np.array(SST.index >= 1988).astype(int)

# flit linear regression model
X = sm.add_constant(Int)

model = sm.OLS(SST["SST"], X).fit()

# model summary to view
print(model.summary())
print("p-value = %s"%(model.pvalues.x1))

# mag: 1.1
# p-value: 4.4e-12

# 5. report AIC of model
print("AIC = %f"%(model.aic))

# 6. Fit a general least squares linear regression onto the SST data as a 
#    function of intervention alone, report the intervention estimate and p-value

model = sm.GLSAR(SST["SST"], X, 1).iterative_fit()

#check acf, pacf of residuals to make sure they have AR(1) structure
plt.figure()
plot_acf(model.resid, title='ACF of Residuals of SST GLS Model')
plt.show()

plt.figure()
plot_pacf(model.resid, title ='PACF of Residuals of SST GLS Model')
plt.show()

# plots show AR(1) structure

# report intervention estimate, p-value
print(model.summary())
print("p-value = %s"%(model.pvalues.x1))

# plot gls to data to check
plt.figure()
SST["SST"].plot(label = "SST Time Series")
model.fittedvalues.plot(label = "GLS Intervention Model")
plt.title("Change in SST over Time")
plt.xlabel("Time (Yr)")
plt.ylabel("SST (Degrees C)")
plt.legend()
plt.show()

# 7. Report AIC
print("AIC = %s"%(model.aic))

# 8. Fit an AR(1) model to the data (w/o intervention) and report the AIC
model = sm.GLSAR(SST["SST"], np.ones(len(SST["SST"])), 1).iterative_fit()

print("AIC = %s"%(model.aic))