# Alex Watson
#------------------------OCEA 267: Problem Set 1------------------------------
# coded in python3

# import relevant modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.seasonal import seasonal_decompose
from scipy.ndimage import uniform_filter1d
from datetime import datetime, timedelta
import statsmodels.api as sm

# 1. Create plot of Greenland Mass Time Series
# read dataframe
gmass = pd.read_csv("greenland_mass_200204_202201.txt" , skiprows=31, header = None, 
                    delim_whitespace=True, names = ['decyear','mass','uncertainty'])

# convert to a time series object
year = np.array(gmass['decyear']).astype(int)
rem = np.array(gmass['decyear'])-year
base = []
dates = []
for i in range(len(year)):
    base.append(datetime(year[i], 1, 1))
    dates.append(base[i] + timedelta(seconds=(base[i].replace(year=base[i].year + 1) - base[i]).total_seconds() * rem[i]))

# dates = datetime(gmass['decyear'])
gmass['decyear'] = dates
gmass.set_index('decyear', inplace=True)

# plot time series
gmass['mass'].plot()
plt.title('Greenland Mass Anomalies vs. Time since April 2002')
plt.xlabel('Time (yr)')
plt.ylabel('Mass (GT)')
plt.savefig('Watson_OCEA267_PS1_1.png', dpi = 150)

# 2. Decompose the Greenland mass time series into a trend + seasonal component + remainder
decomp = seasonal_decompose(gmass["mass"], model = 'additive', period=12)

fig = decomp.plot()
fig.set_size_inches((14,9))
fig.tight_layout()
plt.xlabel("Time (yr)")
plt.savefig('Watson_OCEA267_PS1_2.png', dpi = 150)

# 4. Apply 120 month moving average to Berkeley global land-ocean temperature dataset time series and plot
# read dataframe
blot = pd.read_csv("LOT_berkeley_1850_2021_monthly.txt" , skiprows=86, 
                   header = None, delim_whitespace=True, 
                   names = ['year','month','T_anom'], 
                   usecols=['year','month','T_anom'])

# convert to time series
dates = []
for index, row in blot.iterrows():
    dates.append(f'{int(row.year)}-{int(row.month)}')
dates = pd.to_datetime(dates)
blot['date'] = dates
blot.set_index('date', inplace=True)

# apply 120 month moving average filter
blot['T120'] = uniform_filter1d(blot['T_anom'], size=120)

#plot figure
plt.figure()
blot['T_anom'].plot(label='Temperature Anomaly')
blot['T120'].plot(label='Filtered Temp. Data')
plt.title('Monthly LOT Anomalies vs. Time')
plt.legend()
plt.xlabel('Time (yr)')
plt.ylabel('Temperature Anomaly (C)')
plt.savefig('Watson_OCEA267_PS1_4.png', dpi=150)

# 5. Apply 2,5,20 year moving averages and plot results
#create filtered data
blot['T24'] = uniform_filter1d(blot['T_anom'], size=24)
blot['T60'] = uniform_filter1d(blot['T_anom'], size=60)
blot['T240'] = uniform_filter1d(blot['T_anom'], size=240)

#plot data
plt.figure()
blot['T_anom'].plot(label='Temperature Anomaly')
blot['T24'].plot(label='2-Year Filter')
blot['T60'].plot(label='5-Year Filter')
blot['T120'].plot(label='10-Year Filter')
blot['T240'].plot(label='20-Year Filter')
plt.title('Monthly LOT Anomalies vs. Time')
plt.legend()
plt.xlabel('Time (yr)')
plt.ylabel('Temperature Anomaly (C)')
plt.savefig('Watson_OCEA267_PS1_5.png', dpi=150)

# 7. Read Arctic Ice Sheet Dataset, fit a linear trend model and plot
# read dataframe
arc = pd.read_csv("Sept_Arctic_extent_1979-2021.csv", sep = ',', usecols=['year','extent'])

# fit linear trend model
x = arc["year"].values
X = sm.add_constant(x)
Y = arc["extent"].values

linreg = sm.OLS(Y, X).fit()

# convert to time series
date = []
for i in range(len(arc["year"])):
    date.append(datetime(arc["year"][i], 9, 1))
arc['Date'] = date
arc.set_index('Date', inplace=True)

# add linear regression to dataframe
arc["linreg"] = linreg.fittedvalues

# plot data / results
plt.figure()
arc["extent"].plot(label="Data")
arc["linreg"].plot(label="Linear Regression Model")
plt.legend()
plt.xlabel("Time (Yr)")
plt.ylabel("Ice Sheet Extent (Mkm^2)")
plt.title("Change in Ice Sheet Extent in the Arctic over Time")
plt.savefig("Watson_OCEA267_PS1_7.png", dpi=150)

# 8. What is estimate of trend?
slope = linreg.params[1]
intercept = linreg.params[0]
print("yhat_i = %f + %f*xi"%(intercept, slope))

# 9. Are Assumptions on Residuals accurate?
# residuals
resid = linreg.resid

#plot residuals
plt.figure()
plt.scatter(x, resid)
plt.plot(x, np.zeros(len(x)))
plt.title("Residuals of Linear Regression Over Time")
plt.xlabel("Time")
plt.ylabel("Residuals")
plt.savefig("Watson_OCEA267_PS1_9_1.png", dpi=150)

# plot histogram of residuals
bins = np.linspace(-1.5, 1.5, 13)
plt.figure()
plt.hist(resid, bins=bins)
plt.xlabel("Residuals")
plt.ylabel("Number of Residuals")
plt.title("Histogram of Residuals")
plt.savefig("Watson_OCEA267_PS1_9_2.png", dpi=150)