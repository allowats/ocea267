# Alex Watson
# Week 3 In-Class Exercise (Lecture 6)

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import statsmodels.api as sm
from statsmodels.stats.stattools import durbin_watson
from scipy.stats import shapiro, bartlett


#read GMST NASA data
GMST = pd.read_csv("GMST_1880_2020_NASA.txt", skiprows=5, header=None, 
                   delim_whitespace=True, names=['YR','TEMP','UNC'])

#apply simple linear regresssion model to data
x = GMST["YR"].values
X = sm.add_constant(x)
Y = GMST["TEMP"].values
e = GMST["UNC"].values

reg = sm.OLS(Y, X).fit()

#predictions
predictions = reg.predict(X) 

reg.summary()

#convert to time series
date = []
for i in range(len(GMST["YR"])):
    date.append(datetime(GMST["YR"][i], 1, 1))
GMST['Date'] = date
GMST.set_index('Date', inplace=True)


GMST["linreg"] = reg.fittedvalues
#plot results
plt.figure()
GMST["TEMP"].plot(label="data")
GMST["linreg"].plot(label="Linear Regression Model")
plt.xlabel("Time (Yr)")
plt.ylabel("Temperature (degrees C)")
plt.legend()

# Apply Durbin-Watson test on residuals
dw = durbin_watson(reg.resid)
print(dw)
# autocorrelated residuals

# perform Cochrane-Orcutt transformation on global mean surface temp
# estimate rhohat
top = []
bottom = []
for i in np.arange(1,len(reg.resid),1):
    top.append(reg.resid[i]*reg.resid[i-1])
for i in range(len(reg.resid)):
    bottom.append(reg.resid[i]**2)

rhohat = sum(top)/sum(bottom)

#Cochrane-Orcutt
ystar = []
for i in np.arange(1,len(GMST['TEMP']),1):
    ystar.append(GMST['TEMP'][i]-rhohat*GMST['TEMP'][i-1])
ystar = np.array(ystar)

# fit linear trend to transformed data & plot
GMST_t = GMST[1:]
GMST_t["TRANS"] = ystar
x_t = GMST_t["YR"].values
X_t = sm.add_constant(x_t)
Y_t = GMST_t["TRANS"].values

reg_t = sm.OLS(Y_t, X_t).fit()
GMST_t["linreg_t"] = reg_t.fittedvalues

# plot
plt.figure()
GMST_t["TRANS"].plot(label="Transformed Data")
GMST_t["linreg_t"].plot(label="Linear Regression Model")
plt.xlabel("Time (Yr)")
plt.ylabel("Temperature (degrees C)")
plt.legend()

# apply Durbin-Watson test on residuals
dw_t = durbin_watson(reg_t.resid)
print(dw_t)
# value is between 1.5 and 2.5 -->independent

#Apply Shapiro-Wilk test on residuals for normality
s_t = shapiro(reg_t.resid)
print(s_t[1])
# the p-value is higher than 0.05 and therefore the normality assumption cannot be rejected.

#apply Bartlett test on residuals
res1 = reg_t.resid[0:55]
res2 = reg_t.resid[56:]
b_t = bartlett(res1, res2)
print(b_t[1])

# the p-value is above 0.05 --> variance is constant

plt.figure()
plt.scatter(x_t, reg_t.resid)
plt.plot(x_t, np.zeros(len(x_t)))