# Alex Watson
# Week 7 In-Class Exercise (Lecture 14)

# import modules
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.stats.stattools import durbin_watson
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
from scipy.stats import shapiro, bartlett

# Import Data
aq = pd.read_csv("PM25West.csv")

# plot data
plt.figure()
plt.plot(aq["Year"], aq["Mean"])

# fit OLS
X = sm.add_constant(aq["Year"])

model = sm.OLS(aq["Mean"], X).fit()

#plot ols
plt.figure()
plt.plot(aq["Year"], aq["Mean"])
plt.plot(aq["Year"], model.fittedvalues)
plt.show()

#plot residuals
plt.figure()
plt.scatter(aq["Year"], model.resid)
plt.plot(aq["Year"], np.zeros(len(aq["Year"])), 'k--')
plt.show()

# maybe not independent? try Durbin-Watson

d = durbin_watson(model.resid)
print(d)

# d-statistic too low --> not independendt

# check autocorrelation using acf, pacf
plt.figure()
plot_acf(model.resid)
plt.show()

plt.figure()
plot_pacf(model.resid)
plt.show()
#Try fitting gls instead

model = sm.GLS(aq["Mean"], X).fit()

# plot gls
plt.figure()
plt.plot(aq["Year"], aq["Mean"])
plt.plot(aq["Year"], model.fittedvalues)
plt.show()

# resid normality & constant variance?
#normality --> Shapiro_Wilk
w = shapiro(model.resid)
print(w)

#p value > 0.05 --> residuals normally distributed
# constant var --> bartlett test
resid1 = model.resid[0:10]
resid2 = model.resid[11:]
b = bartlett(resid1, resid2)

print(b)

# p value > 0.05 --> constant variance

