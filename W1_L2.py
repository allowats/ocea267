# Alex Watson
# ---------------------OCEA 267: Exercise W1 L2-------------------------------

import numpy as np
# 1. Create vector with 10 values
vec10 = np.random.rand(10)
# print(vec10)

# 2. Build 4x3 matrix with ones everywhere
ones = np.ones([4,3])
# print(ones)

# 3. Build a 4x3 matrix, assigned to variable A, with the numbers 1 through 3 in each column
A = np.array([[1,2,3],
              [1,2,3],
              [1,2,3],
              [1,2,3]])
# print(A)

# 4. Build 4x3 matrix assigned to B with numbers 1 to 4 in each row
B = np.array([[1,1,1],
              [2,2,2],
              [3,3,3],
              [4,4,4]])
# print(B)

# 5. Extract the elements in the 1st and 2nd rows and 1st and 2nd columns of matrix A created above and assign to matrix S (2x2)
S = A[0:2,0:2]
# print(S)

# 6. Build 2x2 matrix filled with NAs
NA = np.empty([2,2])
NA[:] = np.nan # nan is Python's version of NA
# print(NA)

# 7. Import "ch4_mm_gl.txt"
import pandas as pd

ch4 = pd.read_csv("ch4_mm_gl.txt" , sep = '\\s+')

# 8. Extracvt the ch4 monthly mean (average column), assign to a variable
mm = ch4["average"]
# 9. Turn the ch4 monthly mean into a time series object
dates = []
for index, row in ch4.iterrows():
    dates.append(f'{int(row.year)}-{int(row.month)}')
dates = pd.to_datetime(dates)
ch4['date'] = dates
ch4.set_index('date', inplace=True)

# 10. plot data and label axes
import matplotlib.pyplot as plt
ch4["average"].plot()
plt.xlabel('Time (yr)')
plt.ylabel('Monthly Mean CH4 (units?)')

# 11. perform classical decomposition of the monthly mean ch4 data and plot all components
from statsmodels.tsa.seasonal import seasonal_decompose
decomp = seasonal_decompose(ch4["average"], model = 'additive', period=12)

fig = decomp.plot()
fig.set_size_inches((14,9))
fig.tight_layout()
plt.xlabel("Time (yr)")

# 12. Apply a moving average filter to ch4 monthly time series
from scipy.ndimage import uniform_filter1d
filt_avg = uniform_filter1d(ch4.average, size=7)

ch4['filtered'] = filt_avg
# 13. Compare the trend obtained by moving average filter vs trend obtained by decomposition
plt.figure()
decomp.trend.plot(label='trend')
ch4['filtered'].plot(label='filtered')
plt.title('Seasonal Decomposition Trend vs Moving Average Filter Trend')
plt.legend()
plt.xlabel('Time (yr)')
plt.ylabel('Monthly Mean CH4 (units?)')