# Alex Watson
# Week 6 In-Class Exercise (Lecture 12)

# import modules
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf
from statsmodels.graphics.tsaplots import plot_acf, plot_pacf

# read chl_data_stationX dataset
chl = pd.read_csv("chl_data_stationX.txt", delim_whitespace=True)

# fit a linear regression model to chl data w/ linear trend & 11 dummy variables
# get dummy vars
chl = pd.get_dummies(chl, columns=['month'])

# fit ols
model = smf.ols("y ~ dd + month_1 + month_2 + month_3 + month_4 + month_5 + month_6 + month_7 + month_8 + month_9 + month_10 + month_11", data=chl).fit()

# plot model onto time series
plt.figure()
plt.plot(chl["dd"], chl["y"], label="Chorophyll Data")
plt.plot(chl["dd"], model.fittedvalues, label = "OLS Model")
plt.title("Chlorophyll Concentration over Time")
plt.xlabel("Time (Yr)")
plt.ylabel("Chlorophyll Concentration (mg/m^3)")
plt.show()

# plot ACF, PACF of residuals
plot_acf(model.resid)
plot_pacf(model.resid)
# Autocorrelation, p of order 2

# fit linear regression model using generalized least squares
mgls = smf.gls("y ~ dd + month_1 + month_2 + month_3 + month_4 + month_5 + month_6 + month_7 + month_8 + month_9 + month_10 + month_11", data=chl).fit()

# plot gls model onto time series
plt.figure()
plt.plot(chl["dd"], chl["y"], label="Chorophyll Data")
plt.plot(chl["dd"], mgls.fittedvalues, label = "GLS Model")
plt.title("Chlorophyll Concentration over Time")
plt.xlabel("Time (Yr)")
plt.ylabel("Chlorophyll Concentration (mg/m^3)")
plt.show()

# There is a trend in the time series